(defproject io.example-yann/sample1lein "0.1.0-SNAPSHOT"
  :description "This is a sample clojure project"
  :url "http://example.com/clojureproject"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [taken "0.1.0"]
                 [clj-x256 "0.0.1"]]
  :main sample1lein.core
  :oat [sample1lein.core]
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}
             :dev {:dependencies [[expectations "2.1.8"]]}}
  :plugins [[lein-expectations "0.0.8"]
            [org.apache.maven.wagon/wagon-http "3.0.0"]]
  :signing {:gpg-key "yannmjl@chenpo.io"}
  :deploy-repositories[["snapshot" {:url "http://examples-yann.mycloudrepo.localhost:6123/repositories/lein-snapshot"
                                    :creds :gpg}]
                       ["release" {:url "http://examples-yann.mycloudrepo.localhost:6123/repositories/lein-release"
                                   :creds :gpg}]])

(require 'cemerick.pomegranate.aether)
      (cemerick.pomegranate.aether/register-wagon-factory!
                            "http" #(org.apache.maven.wagon.providers.http.HttpWagon.))
